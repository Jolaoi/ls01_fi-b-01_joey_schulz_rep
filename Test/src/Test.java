
public class Test {

	public static void main(String[] args) {
		System.out.printf("Hallo %-10s", "Oh\n");
		//%10s ist rechtsbündig und macht 10 Leerstellen. "%" ist ein Platzhalter und das was in Klammern steht, wird dann eingefügt
		System.out.printf("|Hallo %10.2f| hab ich gemacht %s\n", 199.9, "Joey");
		//10.2: Der Punkt macht die Zahl zu einer zweistellingen Kommazahl, welche er automatische rundet
		System.out.println("");
		System.out.println("    **    ");
		System.out.println(" *      *");
		System.out.println(" *      *");
		System.out.println("    **    ");

		

	}

}
