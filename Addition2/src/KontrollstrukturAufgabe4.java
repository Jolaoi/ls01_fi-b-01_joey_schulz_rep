import java.util.Scanner;

public class KontrollstrukturAufgabe4 {
	public static void main(String[] args) {
		
		double zahl1 = 0, zahl2 = 0, ergebnis = 0;
		
		char rechnung;
		
		Scanner sc = new Scanner (System.in);
		System.out.println("Geben Sie ihre erste Zahl ein.");
		zahl1 = sc.nextDouble();
		System.out.println("Geben Sie ihre zweite Zahl ein.");
		zahl2 =sc.nextDouble();
		System.out.println("Geben Sie ihre Rechenart ein. Auswahl = +, -, *, /.");
		rechnung = sc.next().charAt(0);
		
		switch (rechnung) {
		
		case '+':
			ergebnis = zahl1 + zahl2;
			break;
		case '-':
			ergebnis = zahl1 - zahl2;
			break;
		case '*':
			ergebnis = zahl1 * zahl2;
			break;
		case '/':
			ergebnis = zahl1 / zahl2;
			break;
			
		}
		System.out.println("Das Ergebnis betr�gt " + ergebnis);
	}
}
