
import java.util.Scanner;
public class Addition2 {
	public static void programmhinweis() {
		System.out.println("Hinweis: ");
		System.out.println("Das Programm addiert 2 eingegebe Zahlen. ");
	}
	public static void Ausgabe(double erg, double zahl1, double zahl2) {
		System.out.println("Ergebnis der Addition ");
		System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
	}
	public static double verarbeitung(double zahl1, double zahl2) {
		double erg = zahl1 + zahl2; //Ergebnis wird zusammengerechnet
		return erg;	//Ergebnis wird zur�ckgegeben
	}
	public static double eingabe(String Text) {
		
		Scanner sc= new Scanner(System.in);
		System.out.println(Text);
		double zahl= sc.nextDouble();
		return zahl;
	}
	public static void main(String[] args) {
		double erg=0.0, zahl1=0.0, zahl2=0.0;
		programmhinweis();
		zahl1=eingabe("1. Zahl:");
		zahl2=eingabe("2. Zahl:");
		erg=verarbeitung(zahl1, zahl2);
		Ausgabe(erg, zahl1, zahl2);
		

	}

}
