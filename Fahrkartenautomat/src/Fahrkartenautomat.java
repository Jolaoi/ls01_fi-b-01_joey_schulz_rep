﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        // Achtung!!!!
        // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
        // =======================================
        double zuZahlenderBetrag;
        int anzahlTickets;
        double rückgabebetrag;

        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gewählten Fahrkarte(n).
        // -----------------------------------
        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
        
        
        // Fahrkartenanzahl
        // ----------------
        anzahlTickets = fahrkartenAnzahl(tastatur);
        

        
        // Geldeinwurf
        // -----------
        rückgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag, anzahlTickets);

        // Fahrscheinausgabe
        // -----------------
        fahrkartenAusgeben();

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgeldAusgeben(rückgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur){
        double zuZahlenderBetrag = 0;
        int Eingabe = 0;
//        System.out.print("Zu zahlender Betrag (EURO): ");
//        zuZahlenderBetrag = tastatur.nextDouble();
        
        System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        System.out.print("Eingabe: ");
       
        
        Eingabe = tastatur.nextInt();
        boolean validEingabe = true;
        
        if (Eingabe != 1 || Eingabe != 2 || Eingabe != 3) {
			validEingabe = false;
		}
        
        while (!validEingabe) {
        	System.out.println("Die gewählte EIngabe entspricht keinem Tarif.");
        	Eingabe = tastatur.nextInt();
        	if (Eingabe == 1 || Eingabe == 2 || Eingabe == 3) {
    			validEingabe = true;
    		}
        }
        
        
        	 switch (Eingabe) {
             
             case 1: zuZahlenderBetrag = 2.90;
             break;
             case 2: zuZahlenderBetrag = 8.60;
             break;
             case 3: zuZahlenderBetrag = 23.50;
             break;
             default: System.out.println("Die gewählte EIngabe entspricht keinem Tarif.");
             
             }
        	 
       
        
       
        
        
        
        return zuZahlenderBetrag;
    }
    
    public static int fahrkartenAnzahl(Scanner tastatur) {
    	int anzahlTickets;
    	
    	System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
   	
        /*  	
    	if (anzahlTickets > 10 || anzahlTickets < 1 ) {
    		System.out.println("Du kannst nur 1 bis 10 Tickets kaufen");
    		System.out.println("Du hast das Limit überschritten daher wird die Ticket Anzahl auf 1 reduziert");
       		anzahlTickets = 1 ;	
    	}
    	
    	*/
    while(anzahlTickets > 10 || anzahlTickets < 1 ) {
    	System.out.println("Du kannst nur 1 bis 10 Tickets kaufen");
		System.out.println("Du hast das Limit überschritten daher wird die Ticket Anzahl auf 1 reduziert");
       	
		System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        
        }
    	
    	return anzahlTickets;
}
    private static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag, int anzahlTickets) {
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTickets)
        {
        	System.out.printf("%s %.2f %s %n", "Noch zu zahlen:", (zuZahlenderBetrag * anzahlTickets - eingezahlterGesamtbetrag), "€" );
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTickets;
    }
    
    private static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0)
        {
        	System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von:", rückgabebetrag, "€");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
            while(rückgabebetrag >= 0.02) // 2 CENT-Münzen
            {
         	  System.out.println("2 CENT");
  	          rückgabebetrag -= 0.02;
            }
            while(rückgabebetrag >= 0.01) // 1 CENT-Münzen
            {
         	  System.out.println("1 CENT");
  	          rückgabebetrag -= 0.01;
            }
            while(rückgabebetrag >= 0.001) // 1 CENT-Münzen
            {
         	  System.out.println("1 CENT");
  	          rückgabebetrag -= 0.01;
            }
        }
    }

    private static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
}